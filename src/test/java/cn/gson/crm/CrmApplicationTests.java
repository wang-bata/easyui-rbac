package cn.gson.crm;

import cn.gson.crm.model.dao.MemberDao;
import cn.gson.crm.model.domain.Member;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CrmApplicationTests {
	@Autowired
	MemberDao memberDao;

	@Test
	public void contextLoads() {
		Member member = memberDao.findOne(1L);
		member.setPassword(DigestUtils.sha256Hex("0000"));
		memberDao.save(member);
	}

}
