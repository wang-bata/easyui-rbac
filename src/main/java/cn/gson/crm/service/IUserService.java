package cn.gson.crm.service;

import cn.gson.crm.bo.MiniChatLoginBo;
import cn.gson.crm.model.domain.UserEntity;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
public interface IUserService {
    /**
     * openId查找当前用户
     * @param openId
     * @return
     */
    UserEntity findUserByOpenId(String openId);

    void save(UserEntity userEntity);

    String miniChatLogin(MiniChatLoginBo miniChatLoginBo);
}
