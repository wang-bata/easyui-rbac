package cn.gson.crm.service;

import cn.gson.crm.model.domain.AddressSnapshotEntity;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
public interface IAddressSnapshotService {
    void save(AddressSnapshotEntity addressSnapshotEntity);
}
