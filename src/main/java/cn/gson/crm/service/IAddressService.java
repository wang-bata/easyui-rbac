package cn.gson.crm.service;

import cn.gson.crm.model.domain.AddressEntity;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
public interface IAddressService {

    void edit(AddressEntity addressEntity);

    List<AddressEntity> getUserAddressList(int id);

    void deleteById(Integer id);

    AddressEntity getAddressEntityById(Integer id);
}
