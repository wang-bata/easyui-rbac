package cn.gson.crm.service;

import cn.gson.crm.bo.OrderBo;
import cn.gson.crm.bo.OrderDetailBo;
import cn.gson.crm.model.domain.OrderEntity;
import java.util.*;
/**
 * Created by wangxiangyun on 2020/4/26.
 */
public interface IOrderService {
    /**
     * 新增订单接口
     * @param orderBo
     */
    void addPro(OrderBo orderBo);

    List<OrderDetailBo> getUserOrderList(Integer userId);
}
