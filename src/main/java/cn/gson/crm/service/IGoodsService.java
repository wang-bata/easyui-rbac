package cn.gson.crm.service;

import cn.gson.crm.model.domain.GoodsEntity;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
public interface IGoodsService {
    List<GoodsEntity> getGoodsByCategoryId(Integer categoryId);

    GoodsEntity getGoodsById(Integer id);
}
