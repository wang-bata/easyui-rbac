package cn.gson.crm.service.impl;

import cn.gson.crm.model.dao.AddressSnapshotDao;
import cn.gson.crm.model.domain.AddressSnapshotEntity;
import cn.gson.crm.service.IAddressSnapshotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
@Service
public class IAddressSnapshotServiceImpl implements IAddressSnapshotService {
    @Autowired
    private AddressSnapshotDao addressSnapshotDao;
    @Override
    public void save(AddressSnapshotEntity addressSnapshotEntity) {
        addressSnapshotEntity.setCreateTime(new Timestamp(new Date().getTime()));
        addressSnapshotDao.save(addressSnapshotEntity);
    }
}
