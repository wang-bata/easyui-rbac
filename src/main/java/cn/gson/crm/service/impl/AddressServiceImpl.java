package cn.gson.crm.service.impl;

import cn.gson.crm.model.dao.AddressDao;
import cn.gson.crm.model.domain.AddressEntity;
import cn.gson.crm.service.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
@Service
public class AddressServiceImpl implements IAddressService {

    @Autowired
    private AddressDao addressDao;

    @Override
    public void edit(AddressEntity addressEntity) {
        Timestamp timestamp=new Timestamp(new Date().getTime());
        if(addressEntity.getId()==0){
            addressEntity.setCreateTime(timestamp);
        }else {
            addressEntity.setUpdateTime(timestamp);
        }
        addressDao.save(addressEntity);
    }

    @Override
    public List<AddressEntity> getUserAddressList(int userId) {
        List<AddressEntity> addressEntityList=  addressDao.findByUserIdAndDeleted(userId,0);
        return addressEntityList;
    }

    @Override
    public void deleteById(Integer id) {
        AddressEntity one = this.getAddressEntityById(id);
        one.setDeleted(1);
        this.edit(one);
    }

    @Override
    public AddressEntity getAddressEntityById(Integer id) {
        AddressEntity one = addressDao.findOne(id);
        return one;
    }
}
