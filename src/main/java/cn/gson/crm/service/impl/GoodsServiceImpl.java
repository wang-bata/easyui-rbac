package cn.gson.crm.service.impl;

import cn.gson.crm.model.dao.GoodsDao;
import cn.gson.crm.model.domain.GoodsEntity;
import cn.gson.crm.service.IGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@Service
public class GoodsServiceImpl implements IGoodsService {
    @Autowired
    private GoodsDao goodsDao;
    @Override
    public List<GoodsEntity> getGoodsByCategoryId(Integer categoryId) {
        List<GoodsEntity> goodsEntityList=   goodsDao.findByCategoryId(categoryId);
        return goodsEntityList;
    }

    @Override
    public GoodsEntity getGoodsById(Integer id) {
        return goodsDao.findOne(id);
    }
}
