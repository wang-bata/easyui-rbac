package cn.gson.crm.service.impl;

import cn.gson.crm.model.dao.GoodsSnapshotDao;
import cn.gson.crm.model.domain.GoodsSnapshotEntity;
import cn.gson.crm.service.IGoodsSnapshotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
@Service
public class GoodsSnapshotServiceImpl implements IGoodsSnapshotService {
    @Autowired
    private GoodsSnapshotDao goodsSnapshotDao;

    @Override
    public void addList(List<GoodsSnapshotEntity> goodsSnapshotEntityList) {
        goodsSnapshotDao.save(goodsSnapshotEntityList);
    }
}
