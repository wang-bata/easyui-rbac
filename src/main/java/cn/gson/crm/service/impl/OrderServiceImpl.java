package cn.gson.crm.service.impl;

import cn.gson.crm.bo.GoodsAddBo;
import cn.gson.crm.bo.OrderBo;
import cn.gson.crm.bo.OrderDetailBo;
import cn.gson.crm.bo.OrderSnapshotBo;
import cn.gson.crm.model.dao.OrderDao;
import cn.gson.crm.model.domain.*;
import cn.gson.crm.service.*;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@Service
public class OrderServiceImpl implements IOrderService {
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private IGoodsService iGoodsService;
    @Autowired
    private IGoodsSnapshotService iGoodsSnapshotService;
    @Autowired
    private IAddressService iAddressService;
    @Autowired
    private IAddressSnapshotServiceImpl iAddressSnapshotService;

    @Override
    @Transactional
    public void addPro(OrderBo orderBo) {
        Timestamp currentDate = new Timestamp(new Date().getTime());
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderStaus(orderBo.getStatus());
        orderEntity.setCreateTime(currentDate);
        orderEntity.setUserId(orderBo.getUserId());
        orderEntity.setDeleted(0);
        orderEntity.setSnapshot("");
        //先保存获取ID
        orderDao.save(orderEntity);
        List<GoodsAddBo> goodsAddBoList = orderBo.getGoodsAddBoList();
        List<GoodsSnapshotEntity> goodsSnapshotEntityList = new ArrayList<>();
        Double totalPrice = 0d;
        for (GoodsAddBo goodsAddBo : goodsAddBoList) {
            GoodsSnapshotEntity goodsSnapshotEntity = new GoodsSnapshotEntity();
            GoodsEntity goodsEntity = iGoodsService.getGoodsById(goodsAddBo.getId());
            goodsSnapshotEntity.setCategoryId(goodsEntity.getCategoryId());
            goodsSnapshotEntity.setName(goodsEntity.getName());
            goodsSnapshotEntity.setMarketPrice(goodsEntity.getMarketPrice());
            goodsSnapshotEntity.setNumber(goodsAddBo.getNumber());
            goodsSnapshotEntity.setCreateTime(currentDate);
            goodsSnapshotEntity.setUpdateTime(currentDate);
            goodsSnapshotEntity.setOrderId(orderEntity.getId());
            goodsSnapshotEntityList.add(goodsSnapshotEntity);
            totalPrice = totalPrice + (goodsEntity.getMarketPrice() * goodsAddBo.getNumber());
        }
        OrderSnapshotBo orderSnapshotBo=new OrderSnapshotBo();
        orderSnapshotBo.setGoodsSnapshotEntityList(goodsSnapshotEntityList);
        AddressEntity addressEntity = iAddressService.getAddressEntityById(orderBo.getAddressId());
        AddressSnapshotEntity addressSnapshotEntity=new AddressSnapshotEntity();
        BeanUtils.copyProperties(addressEntity,addressSnapshotEntity);
        addressSnapshotEntity.setOrderId(orderEntity.getId());
        orderEntity.setSnapshot(JSONObject.toJSONString(orderSnapshotBo));
        orderEntity.setPrice(totalPrice);
        iAddressSnapshotService.save(addressSnapshotEntity);
        orderDao.save(orderEntity);
        iGoodsSnapshotService.addList(goodsSnapshotEntityList);
    }

    @Override
    public List<OrderDetailBo> getUserOrderList(Integer userId) {
        List<OrderDetailBo> list =new ArrayList<>();
        List<OrderEntity> orderEntityList = orderDao.findByUserIdAndDeleted(userId, 0);
        if(!CollectionUtils.isEmpty(orderEntityList)){
            for (OrderEntity orderEntity : orderEntityList) {
                OrderDetailBo orderDetailBo=new OrderDetailBo();
                BeanUtils.copyProperties(orderEntity,orderDetailBo);
                List<GoodsSnapshotEntity> goodsSnapshotEntityList = JSONObject.parseArray(orderDetailBo.getSnapshot(), GoodsSnapshotEntity.class);
                orderDetailBo.setGoodsSnapshotEntityList(goodsSnapshotEntityList);
                list.add(orderDetailBo);
            }
        }
        return list;
    }
}
