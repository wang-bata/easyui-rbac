package cn.gson.crm.service.impl;

import cn.gson.crm.model.dao.CategoryDao;
import cn.gson.crm.model.domain.CategoryEntity;
import cn.gson.crm.service.ICategoryService;
import cn.gson.crm.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@Service
public class CategoryServiceImpl implements ICategoryService {
    @Autowired
    private CategoryDao categoryDao;

    @Override
    public List<CategoryEntity> getCategoryList() {
        Iterable<CategoryEntity> categoryEntities = categoryDao.findAll();
        return (List<CategoryEntity>) categoryEntities;
    }

    @Override
    public void addiCategory(CategoryEntity categoryEntity) {
        categoryEntity.setDeleted(0);
        categoryEntity.setCreateTime(DateUtil.getCurrentDate());
        categoryDao.save(categoryEntity);
    }


    @Override
    public void editCategory(CategoryEntity categoryEntity) {
        categoryEntity.setUpdateTime(DateUtil.getCurrentDate());
        categoryDao.save(categoryEntity);
    }
}
