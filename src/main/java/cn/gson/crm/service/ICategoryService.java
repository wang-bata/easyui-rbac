package cn.gson.crm.service;

import cn.gson.crm.model.domain.CategoryEntity;
import java.util.*;
/**
 * Created by wangxiangyun on 2020/4/26.
 */
public interface ICategoryService {
    List<CategoryEntity> getCategoryList();

    void addiCategory(CategoryEntity categoryEntity);

    void editCategory(CategoryEntity categoryEntity);
}
