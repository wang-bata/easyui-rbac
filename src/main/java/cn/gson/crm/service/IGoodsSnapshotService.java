package cn.gson.crm.service;

import cn.gson.crm.model.domain.GoodsSnapshotEntity;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
public interface IGoodsSnapshotService {

    void addList(List<GoodsSnapshotEntity> goodsSnapshotEntityList);
}
