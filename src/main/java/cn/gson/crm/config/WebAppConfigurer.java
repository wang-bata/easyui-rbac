package cn.gson.crm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebAppConfigurer extends WebMvcConfigurerAdapter {

	@Autowired
	private WechatMiniInterceptor wechatMiniInterceptor;

	/**
	 * 添加拦截器
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		// 注册登录拦截器
		InterceptorRegistration loginReg = registry.addInterceptor(new LoginInterceptor());
		loginReg.addPathPatterns("/**");
		loginReg.excludePathPatterns("/login","/error","/api/**");

		// 注册权限拦截器
		InterceptorRegistration authReg = registry.addInterceptor(new AuthInterceptor());
		authReg.addPathPatterns("/**");
		// 不受权限控制的请求
		authReg.excludePathPatterns("/", "/login","/error","/api/**");
		// 注册权限拦截器
		InterceptorRegistration wechatReg = registry.addInterceptor(wechatMiniInterceptor);
		wechatReg.addPathPatterns("/api/**");
		wechatReg.excludePathPatterns("/api/user/miniChatLogin","/api/category/list");

		super.addInterceptors(registry);
	}

	@Bean
	public RestTemplate restTemplate(ClientHttpRequestFactory factory){
		return new RestTemplate(factory);
	}

	@Bean
	public ClientHttpRequestFactory simpleClientHttpRequestFactory(){
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		factory.setReadTimeout(5000);//单位为ms
		factory.setConnectTimeout(5000);//单位为ms
		return factory;
	}

}
