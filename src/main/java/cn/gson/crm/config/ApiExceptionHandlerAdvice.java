package cn.gson.crm.config;
import cn.gson.crm.exception.MiniApiException;
import cn.gson.crm.utils.ResultJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

/**
 * Created by wangxiangyun on 2017/1/24.
 */
@ControllerAdvice(annotations = RestController.class)
public class ApiExceptionHandlerAdvice {
    Logger logger = LoggerFactory.getLogger(ApiExceptionHandlerAdvice.class);
    /**
     * Handle exceptions thrown by handlers.
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResultJson exception(Exception exception, WebRequest request) {
        ResultJson errorResult =new ResultJson();
        errorResult.setMessage(exception.getMessage());
        errorResult.setCode(-1);
        logger.error("小程序系统异常：",exception);
        return errorResult;
    }
    @ExceptionHandler(value = MiniApiException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResultJson miniApiException(MiniApiException mniApiException, WebRequest request) {
        ResultJson errorResult =new ResultJson();
        errorResult.setMessage(mniApiException.getMessage());
        errorResult.setCode(1);
        logger.error("小程序业务异常:",mniApiException);
        return errorResult;
    }
}