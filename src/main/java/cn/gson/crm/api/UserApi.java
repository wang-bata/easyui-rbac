package cn.gson.crm.api;

import cn.gson.crm.bo.MiniChatLoginBo;
import cn.gson.crm.exception.MiniApiException;
import cn.gson.crm.model.domain.UserEntity;
import cn.gson.crm.service.IUserService;
import cn.gson.crm.utils.ResultJson;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@RestController
@RequestMapping("/api/user")
@Slf4j
public class UserApi extends BaseApi {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private IUserService iUserService;

    private final String tokenHeader = "Authorization";

    @PostMapping("/miniChatLogin")
    public ResultJson miniChatLogin(@RequestBody MiniChatLoginBo miniChatLoginBo) {
        String existsToken = request.getHeader(tokenHeader);
        if (!StringUtils.isEmpty(existsToken)) {
            String userInfoStr = stringRedisTemplate.opsForValue().get(existsToken);
            if (!StringUtils.isEmpty(userInfoStr)) {
                throw new MiniApiException("用户已经登陆");
            }
        }
        String token = iUserService.miniChatLogin(miniChatLoginBo);
        miniChatLoginBo.setCode(token);
        log.info("用户的token是：{}",token);
        return new ResultJson(token);
    }

}
