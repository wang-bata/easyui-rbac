package cn.gson.crm.api;

import cn.gson.crm.model.domain.CategoryEntity;
import cn.gson.crm.model.domain.GoodsEntity;
import cn.gson.crm.service.IGoodsService;
import cn.gson.crm.utils.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@RestController
@RequestMapping("/api/goods")
public class GoodsApi {

    @Autowired
    private IGoodsService iGoodsService;

    @GetMapping("/list/{categoryId}")
    public ResultJson getCategoryList(@PathVariable Integer categoryId) {
        List<GoodsEntity> goodsEntityList = iGoodsService.getGoodsByCategoryId(categoryId);
        return new ResultJson(goodsEntityList);
    }
}
