package cn.gson.crm.api;

import cn.gson.crm.bo.GoodsAddBo;
import cn.gson.crm.bo.OrderBo;
import cn.gson.crm.bo.OrderDetailBo;
import cn.gson.crm.model.domain.GoodsEntity;
import cn.gson.crm.model.domain.UserEntity;
import cn.gson.crm.service.IOrderService;
import cn.gson.crm.utils.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@RestController
@RequestMapping("/api/order")
public class OrderApi extends BaseApi {

    @Autowired
    private IOrderService iOrderService;

    @PostMapping("add")
    public ResultJson add(@RequestBody OrderBo orderBo) {
        iOrderService.addPro(orderBo);
        return new ResultJson();
    }
    @GetMapping("list")
    public ResultJson listbyUserId() {
        UserEntity sessionUser = this.getSessionUser();
        List<OrderDetailBo> userOrderList = iOrderService.getUserOrderList(sessionUser.getId());
        return new ResultJson(userOrderList);
    }
}
