package cn.gson.crm.api;

import cn.gson.crm.model.domain.CategoryEntity;
import cn.gson.crm.service.ICategoryService;
import cn.gson.crm.utils.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@RestController
@RequestMapping("/api/category")
public class CategoryApi {
    @Autowired
    private ICategoryService iCategoryService;

    @GetMapping("/list")
    public ResultJson getCategoryList() {
        List<CategoryEntity> categoryList = iCategoryService.getCategoryList();
        return new ResultJson(categoryList);
    }

}
