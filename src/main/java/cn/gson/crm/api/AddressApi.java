package cn.gson.crm.api;

import cn.gson.crm.model.domain.AddressEntity;
import cn.gson.crm.service.IAddressService;
import cn.gson.crm.utils.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
@RestController
@RequestMapping("/api/address")
public class AddressApi extends BaseApi {

    @Autowired
    private IAddressService iAddressService;

    @PostMapping("edit")
    public ResultJson edit(@RequestBody AddressEntity addressEntity) {
        addressEntity.setUserId(getSessionUser().getId());
        iAddressService.edit(addressEntity);
        return new ResultJson();
    }

    @GetMapping("list")
    public ResultJson list() {
        List<AddressEntity> addressEntityList = iAddressService.getUserAddressList(getSessionUser().getId());
        return new ResultJson(addressEntityList);
    }

    @PostMapping("delete/{id}")
    public ResultJson delete(@PathVariable Integer id) {
        iAddressService.deleteById(id);
        return new ResultJson();
    }

}
