package cn.gson.crm.api;

import cn.gson.crm.exception.MiniApiException;
import cn.gson.crm.model.domain.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by wangxiangyun on 2020/4/26.
 */

public class BaseApi {

    @Autowired
    HttpServletRequest request;

    public UserEntity getSessionUser() {
        Object userInfo = request.getSession().getAttribute("userInfo");
        if(userInfo==null){
           throw new MiniApiException("请登录");
        }
        UserEntity user = (UserEntity) userInfo;
        return user;
    }
}
