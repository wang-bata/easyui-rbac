package cn.gson.crm.model.dao;

import cn.gson.crm.model.domain.GoodsEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@Repository
public interface GoodsDao extends PagingAndSortingRepository<GoodsEntity, Integer> {
    List<GoodsEntity> findByCategoryId(Integer categoryId);
}
