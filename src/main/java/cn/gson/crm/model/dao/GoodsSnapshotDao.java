package cn.gson.crm.model.dao;

import cn.gson.crm.model.domain.GoodsSnapshotEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
public interface GoodsSnapshotDao  extends PagingAndSortingRepository<GoodsSnapshotEntity, Long> {
}
