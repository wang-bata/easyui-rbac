package cn.gson.crm.model.dao;

import cn.gson.crm.model.domain.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@Repository
public interface UserDao extends PagingAndSortingRepository<UserEntity, Long> {
    UserEntity findByOpenIdAndDeleted(String openId, int deleted);
}
