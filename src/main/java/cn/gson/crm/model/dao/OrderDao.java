package cn.gson.crm.model.dao;

import cn.gson.crm.model.domain.OrderEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@Repository
public interface OrderDao  extends PagingAndSortingRepository<OrderEntity, Long> {
    List<OrderEntity> findByUserIdAndDeleted(Integer userId, int deleted);
}
