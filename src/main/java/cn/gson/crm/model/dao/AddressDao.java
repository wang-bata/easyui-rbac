package cn.gson.crm.model.dao;

import cn.gson.crm.model.domain.AddressEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@Repository
public interface AddressDao  extends PagingAndSortingRepository<AddressEntity, Integer>  {
    List<AddressEntity> findByUserIdAndDeleted(int userId, int i);
}
