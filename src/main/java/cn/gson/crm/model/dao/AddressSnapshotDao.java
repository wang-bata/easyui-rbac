package cn.gson.crm.model.dao;

import cn.gson.crm.model.domain.AddressSnapshotEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
@Repository
public interface AddressSnapshotDao  extends PagingAndSortingRepository<AddressSnapshotEntity, Integer> {
}
