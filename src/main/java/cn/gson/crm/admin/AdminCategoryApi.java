package cn.gson.crm.admin;

import cn.gson.crm.common.AjaxResult;
import cn.gson.crm.common.DataGrid;
import cn.gson.crm.model.domain.CategoryEntity;
import cn.gson.crm.model.domain.Member;
import cn.gson.crm.service.ICategoryService;
import cn.gson.crm.utils.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Created by wangxiangyun on 2020/5/6.
 */
@Controller
@RequestMapping("/admin/category")
public class AdminCategoryApi {

    @Autowired
    private ICategoryService iCategoryService;

    @RequestMapping("/index")
    public String index() {
        return  "admin/category/category";
    }

    @RequestMapping("/list")
    @ResponseBody
    public DataGrid<CategoryEntity> list() {
        List<CategoryEntity> categoryList = iCategoryService.getCategoryList();
        DataGrid dataGrid=new    DataGrid((long) categoryList.size(), categoryList);
        return dataGrid;

    }

    @RequestMapping("/add")
    @ResponseBody
    public AjaxResult addCategoryList( CategoryEntity categoryEntity) {
        iCategoryService.addiCategory(categoryEntity);
        return new AjaxResult();
    }

    @RequestMapping("/edit")
    @ResponseBody
    public AjaxResult editCategoryList( CategoryEntity categoryEntity) {
        iCategoryService.editCategory(categoryEntity);
        return new AjaxResult();
    }
}
