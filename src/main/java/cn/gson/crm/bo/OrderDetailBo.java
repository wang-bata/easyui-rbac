package cn.gson.crm.bo;

import cn.gson.crm.model.domain.GoodsSnapshotEntity;
import lombok.Data;

import java.util.*;
import java.sql.Timestamp;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
@Data
public class OrderDetailBo {
    private int id;
    private Integer userId;
    private Integer orderStaus;
    private String snapshot;
    private Double price;
    private Integer deleted;
    private Timestamp createTime;
    private Timestamp updateTime;
    private List<GoodsSnapshotEntity> goodsSnapshotEntityList;
}
