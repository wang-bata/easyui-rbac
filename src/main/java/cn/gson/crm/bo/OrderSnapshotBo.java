package cn.gson.crm.bo;

import cn.gson.crm.model.domain.GoodsSnapshotEntity;
import lombok.Data;

import java.util.List;

/**
 * Created by wangxiangyun on 2020/4/27.
 */
@Data
public class OrderSnapshotBo {
    private List<GoodsSnapshotEntity> goodsSnapshotEntityList;
}
