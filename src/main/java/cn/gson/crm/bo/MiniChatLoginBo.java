package cn.gson.crm.bo;

import lombok.Data;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@Data
public class MiniChatLoginBo {
    private String code;
    private String avatarUrl;
    private int gender;
    private String nickName;
    private String language;
    private String city;
    private String province;
    private String country;
    private Long merchantId;
}
