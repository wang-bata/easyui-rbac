package cn.gson.crm.bo;

import lombok.Data;

import java.util.*;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@Data
public class OrderBo {
    private List<GoodsAddBo> goodsAddBoList;
    private Integer addressId;
    private Integer userId;
    /**
     * 0:待下单，1：以下单，2：已付款
     */
    private Integer status = 1;
}
