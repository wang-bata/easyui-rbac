package cn.gson.crm.bo;

import lombok.Data;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
@Data
public class GoodsAddBo {
    private Integer id;
    private Integer number=0;
}
