package cn.gson.crm.exception;

/**
 * Created by wangxiangyun on 2020/4/26.
 */
public class MiniApiException extends RuntimeException {
    public MiniApiException(String message) {
        super(message);
    }
}
