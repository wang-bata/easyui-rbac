package cn.gson.crm.utils;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by wangxiangyun on 2020/5/6.
 */
public class DateUtil {
    public static Timestamp  getCurrentDate(){
       return new Timestamp(new Date().getTime());
    }
}
