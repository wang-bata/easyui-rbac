<div class="easyui-layout" fit="true">
    <div data-options="region:'north',border:false" style="height: 80px;padding: 10px;overflow: hidden;" title="餐饮类别">
        <form id="member_search_from" class="searcher-form">
            <input name="userName" class="easyui-textbox field" label="类别名称：" labelWidth="80">
            <a class="easyui-linkbutton searcher" data-options="iconCls:'fa fa-search'">检索</a>
            <a class="easyui-linkbutton reset" data-options="iconCls:'fa fa-repeat'">重置</a>
        </form>
    </div>
    <div data-options="region:'center',border:false" style="border-top: 1px solid #D3D3D3">
        <table id="member_dg"></table>
    </div>
</div>

<script>
    $(function () {
        var dg = $("#member_dg");
        var searchFrom = $("#member_search_from");
        var form;

        // 使用edatagrid，需要而外导入edatagrid扩展
        dg.datagrid({
                        url: '/admin/category/list',
                        emptyMsg: "还未创建用户",
                        idField: "id",
                        fit: true,
                        rownumbers: true,
                        fitColumns: true,
                        border: false,
                        pagination: true,
                        singleSelect: true,
                        ignore: ['roles'],
                        pageSize: 30,
                        columns: [[{
                            field: 'name',
                            title: '类别名称',
                            width: 30,
                            editor: {
                                type: 'validatebox',
                                options: {
                                    required: true
                                }
                            }
                        },{
                            field: 'imageUrl',
                            title: '图片链接',
                            width: 30,
                            editor: {
                                type: 'validatebox',
                                options: {
                                    required: true
                                }
                            }
                        },{
                            field: 'deleted',
                            title: '是否删除',
                            width: 30,
                            editor: {
                                type: 'validatebox',
                                options: {
                                    required: true
                                }
                            },
                            formatter: function (val, row) {
                                return val ? "是" : "否";
                            }
                        },
                            {
                                field: 'test',
                                title: '操作',
                                width: 50,
                                align: 'center',
                                formatter: function (value, row, index) {
                                    return authToolBar({
                                                           "member-edit": '<a data-id="' + row.id + '" class="ctr ctr-edit">编辑</a>',
                                                           "member-delete": '<a data-id="' + row.id + '" class="ctr ctr-delete">删除</a>'
                                                       }).join("");
                                }
                            }
                        ]],
                        toolbar: authToolBar({
                                                 "member-create": {
                                                     iconCls: 'fa fa-plus-square',
                                                     text: "创建",
                                                     handler: function () {
                                                         createForm()
                                                     }
                                                 }

                                             })
                    });


        /**
         * 操作按钮绑定事件
         */
        dg.datagrid("getPanel").on('click', "a.ctr-edit", function () {// 编辑按钮事件
            createForm.call(this, this.dataset.id)
        }).on('click', "a.ctr-delete", function () {// 删除按钮事件
            var id = this.dataset.id;
            $.messager.confirm("删除提醒", "确认删除此用户?", function (r) {
                if (r) {
                    $.get("/admin/category/delete", {id: id}, function () {
                        // 数据操作成功后，对列表数据，进行刷新
                        dg.datagrid("reload");
                    });
                }
            });
        });

        /**
         * 搜索区域事件
         */
        searchFrom.on('click', 'a.searcher', function () {//检索
            dg.datagrid('load', searchFrom.formToJson());
        }).on('click', 'a.reset', function () {//重置
            searchFrom.form('reset');
            dg.datagrid('load', {});
        });


        /**
         * 创建表单窗口
         *
         * @returns
         */
        function createForm(id) {
            var dialog = $("<div/>", {class: 'noflow'}).dialog({
                                                                   title: (id ? "编辑" : "创建") + "类别",
                                                                   iconCls: 'fa ' + (id ? "fa-edit" : "fa-plus-square"),
                                                                   height: id ? 380 : 420,
                                                                   width: 420,
                                                                   href: '/admin/category/edit',
                                                                   queryParams: {
                                                                       id: id
                                                                   },
                                                                   modal: true,
                                                                   onClose: function () {
                                                                       $(this).dialog("destroy");
                                                                   },
                                                                   onLoad: function () {
                                                                       //窗口表单加载成功时执行
                                                                       form = $("#category-form");
                                                                   },
                                                                   buttons: [{
                                                                       iconCls: 'fa fa-save',
                                                                       text: '保存',
                                                                       handler: function () {
                                                                           if (form.form('validate')) {
                                                                               $.post("/admin/category/add", form.serialize(), function (res) {
                                                                                   dg.datagrid('reload');
                                                                                   dialog.dialog('close');
                                                                               })
                                                                           }
                                                                       }
                                                                   }]
                                                               });
        }
    });

</script>