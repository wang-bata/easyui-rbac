<form class="app-form" id="category-form">
  <input type="hidden" name="id">

  <div class="field">
    <input class="easyui-textbox" name="name" style="width:100%" data-options="label:'类别名称：',required:true">
  </div>
  <div class="field">
    <input class="easyui-textbox" name="imageUrl" style="width:100%" data-options="label:'图片链接：',required:true">
  </div>
  <div class="field">
    <input class="easyui-datebox" name="deleted" style="width:100%" data-options="label:'是否删除：',editable:false">
  </div>

</form>
<script>
	<#if member??>
    $(function () {
      //需要延迟一点执行，等待页面所有组件都初始化好，再执行数据初始化
      setTimeout(function () {
        var member = ${member};
        if (member.roles) {
          var roles = [];
          $.each(member.roles, function () {
            roles.push(this.id);
          });
          member.roles = roles.join(",");
        }
        $("#member-form").form("load", member);
      }, 200);
    });
	</#if>
</script>